
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/pause:3.1
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/pause:3.1 k8s.gcr.io/pause:3.1
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/defaultbackend:1.4
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:1.1.3
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kubernetes-dashboard-amd64:v1.8.3
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-proxy-amd64:v1.11.2
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-apiserver-amd64:v1.11.2
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-controller-manager-amd64:v1.11.2
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-scheduler-amd64:v1.11.2

docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/nginx-ingress-controller:0.17.1
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/nginx-ingress-controller:0.17.1 quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.17.1

docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/elasticsearch:v6.2.5
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/fluentd-elasticsearch:v2.2.0

docker pull  docker.io/haproxy:1.7-alpine
docker pull  docker.io/osixia/keepalived:1.4.5

docker pull  registry.cn-hangzhou.aliyuncs.com/google_containers/prometheus-operator:v0.22.0
docker tag  registry.cn-hangzhou.aliyuncs.com/google_containers/prometheus-operator:v0.22.0  quay.io/coreos/prometheus-operator:v0.22.0

docker pull mintel/kibana-oss
docker tag mintel/kibana-oss docker.elastic.co/kibana/kibana-oss:6.2.2
