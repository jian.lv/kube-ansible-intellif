# Vagrant machine variables
$master_count = 3
$node_count = 2
$system_vcpus = 2
$system_memory = 2048
$bridge_enable = false
$bridge_eth = "ens33"
$private_subnet = "172.16.35"
$net_count = 10
